package main

import (
	"github.com/gen2brain/raylib-go/raylib"
)

var screenWidth int32 = 800
var screenHeight int32 = 450
var renderSize int32 = 1
var renderScale int32 = 16

type biome struct {
	name string
	color rl.Color
}

type chunk struct {
	x int32
	y int32
	biome biome
}

func generateChunk(gridX int32, gridY int32) {
	size := renderSize * renderScale
	x := (gridX * size) + (screenWidth / 2) - (size / 2)
	y := (gridY * size) + (screenHeight / 2) - (size / 2)
	rl.DrawRectangle(x, y, size, size, rl.Blue)
}

func main() {
	rl.SetConfigFlags(rl.FlagVsyncHint)
	rl.InitWindow(screenWidth, screenHeight, "rogue")
	rl.SetTargetFPS(60)

	for !rl.WindowShouldClose() {
		rl.BeginDrawing()
		rl.ClearBackground(rl.RayWhite)
		generateChunk(0, 0)
		generateChunk(1, 0)
		generateChunk(2, 0)
		generateChunk(0, 1)
		generateChunk(0, 2)
		generateChunk(-10, 20)
		rl.EndDrawing()
	}

	rl.CloseWindow()
}
